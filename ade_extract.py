import os
from collections import Counter

from train_model import ADE_Extractor, read_data, SEQUENCE_LEN, MODELS_DIR
from preprocessing import Preprocess

import numpy as np
from keras.models import load_model
from sklearn.metrics import f1_score, precision_score, recall_score


def extract_ades(model, texts, threshold):
    print("Подсчет результатов")
    res = []
    for i, text in enumerate(texts):
        print("Предсказание на %s-й записи" % i)
        voting_per_word = [[] for word in text]
        for i in range(len(text) - SEQUENCE_LEN):
            text_slice = text[i: i + SEQUENCE_LEN]
            converted_slice = np.expand_dims(np.array(text_slice), axis=0)
            labels = model.predict(converted_slice)[0]
            labels = np.uint8(labels >= threshold)

            for j, label in enumerate(labels):
                voting_per_word[i + j].append(label)

        voting_per_word = [Counter(votes).most_common(1) for votes in voting_per_word]
        voting_result = [most_common_vote[0][0] if most_common_vote else [] for most_common_vote in voting_per_word]
        res.append(voting_result)
    
    return res


if __name__ == '__main__':
    data = read_data("data.json")
    texts = [' '.join(drug['original']) for name, drug in data.items()]
    extracted_ades = [drug['extracted'] for name, drug in data.items()]
    texts = texts[:500]
    extracted_ades = extracted_ades[:500]
    

    # Предподготавливаем данные
    preprocesser = Preprocess(SEQUENCE_LEN)
    word_sequences = preprocesser.preprocess(texts)
    sequences = preprocesser.tokenize(word_sequences, load_tokenizer=True)
    labels = preprocesser.get_labels(word_sequences, extracted_ades)

    # Загружаем модель
    model = load_model(os.path.join(MODELS_DIR,
                                    "MODEL:better_extractor_with_manually_marked_up_data1_LR:0.001_EPOCHS:300_DROPOUT:0.2_BATCHSIZE:128_WORDDIM:100.model"))
    
    # Размечаем побочные эффекты
    labeled_result = extract_ades(model, sequences, threshold=0.15)

    # Вывод результатов
    all_pred_labels, all_true_labels = [], []
    for text, pred_labels, true_labels in zip(word_sequences, labeled_result, labels):
        pred = pred_labels
        pred = list(map(lambda x: 0 if x == [] else x, pred))
        true = true_labels.astype(np.int64).tolist()
        all_pred_labels += pred
        all_true_labels += true

        # print("Предподготовленный текст:", ' '.join(text))
        # print("Разметка:", pred)
        # print("Действительные значения:", true)
    
    print("----- Результаты классификации -----")
    print("F-мера: %s" % f1_score(all_true_labels, all_pred_labels))
    print("Precision: %s" % precision_score(all_true_labels, all_pred_labels))
    print("Recall: %s" % recall_score(all_true_labels, all_pred_labels))
