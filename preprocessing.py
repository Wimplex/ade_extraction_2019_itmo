import re, pickle
import numpy as np

from pymorphy2.analyzer import MorphAnalyzer
from keras.preprocessing.text import Tokenizer

from pprint import pprint

class Preprocess:
    def __init__(self, sequence_len):
        self.sequences = None
        self.vocab_size = None
        self.index_word = None
        self.words_sequences = None
        self.label_sequences = None
        self.sequence_len = sequence_len

    def preprocess(self, original_texts):
        print("Предобработка данных")

        # Извлекаем знаки .,!? как отдельные единицы языка
        texts = map(lambda text: re.split("([.,!?:])", text), original_texts)
        texts = map(lambda text: " ".join(text), texts)

        # Очищаем текст от круглых скобок и прочих знаков препинания,
        # оставляя только те, которые перечисленны выше
        texts = map(lambda text: re.sub(r'\([^)]*\)', '', text), texts)
        texts = map(lambda text: re.findall(r"[а-яА-Я -,\.!\?:]+", text), texts)
        texts = map(lambda text: ' '.join(text), texts)
        texts = map(lambda text: re.sub(r'[\"\#\$\%\&\(\)\*\+\/\;\=\@\[\\\]\^\_\`\{\|\}\~\t\n\”\…]+', '', text), texts)

        # Приводим текст в нижний регистр,
        # заменяем дублирования пробелов на одиночные пробелы
        texts = map(lambda text: text.lower(), texts)
        texts = map(lambda text: re.sub(" +", " ", text), texts)

        # Разделяем описания побочных препаратов по пробелам
        texts = map(lambda text: text.strip().split(' '), texts)

        # Приводим в нормальные формы
        analyzer = MorphAnalyzer()
        texts = map(lambda text: [analyzer.normal_forms(word)[0] for word in text], texts)
        texts = list(texts)

        return texts

    def tokenize(self, word_sequences, load_tokenizer=False):
        # Соединяем в предложения каждый текст
        texts = map(lambda text: ' '.join(text), word_sequences)
        texts = list(texts)

        if load_tokenizer:
            self.load_tokenizer()
        else:
            self.tokenizer = Tokenizer(filters='"#$%&()*+-/;=@[\\]^_`{|}~\t\n”…', oov_token="NotInVocabulary")
            self.tokenizer.fit_on_texts(texts)
        
        words_sequences = self.tokenizer.texts_to_sequences(texts)
        return np.array(words_sequences)

    def get_labels(self, texts, ades):
        # Создаем матрицу меток,
        # где 1 - словоформа является симптомом (или находится в составе словосочетания-симптома),
        # а 0 - не является.
        # Программная структура снизу находит все подпоследовательности слов,
        # которые могут составлять побочный эффект, и сравнивают их с имеющимися побочными эффектами
        labels = [np.zeros(len(text)) for text in texts]
        for i in range(len(texts)):
            curr_text = texts[i]
            curr_label = labels[i]
            curr_ade = ades[i]
            for side_effect in curr_ade:
                len_sd_eff = len(side_effect.split(' '))
                for j in range(len(curr_text) - len_sd_eff):
                    text_slice = curr_text[j: j + len_sd_eff]
                    text_slice = ' '.join(text_slice)
                    if text_slice == side_effect:
                        if not any(curr_label[j: j + len_sd_eff]):
                            curr_label[j: j + len_sd_eff] = 1.0
        return labels

    def fit_on_texts(self, original_texts, ades, load_tokenizer=False):
        # Очищаем и разделяем данные
        texts = self.preprocess(original_texts)

        # Получаем метки из текстов на основе данных в переменной ades
        labels = self.get_labels(texts, ades)

        words_sequences = []
        label_sequences = []
        for text, label in zip(texts, labels):
            for i in range(len(text) - self.sequence_len):
                words_sequences.append(text[i: i + self.sequence_len])
                label_sequences.append(label[i: i + self.sequence_len])

        print("Создано последовательностей длины %s: %s" % (self.sequence_len, len(words_sequences)))

        words_sequences = self.tokenize(words_sequences, load_tokenizer=load_tokenizer)
        self.save_tokenizer()
        self.index_word = self.tokenizer.index_word
        self.vocab_size = len(self.tokenizer.word_index) + 1
        self.words_sequences = words_sequences
        self.label_sequences = label_sequences
        return words_sequences, np.array(label_sequences)

    def fit_on_prepared_data(self, lines, labels, load_tokenizer=False):
        words_sequences = []
        label_sequences = []
        for text, label in zip(lines, labels):
            for i in range(len(text) - self.sequence_len):
                sub_seq_words = text[i: i + self.sequence_len]
                sub_seq_label = label[i: i + self.sequence_len]

                if len(sub_seq_words) == self.sequence_len and len(sub_seq_label) == self.sequence_len:
                    words_sequences.append(sub_seq_words)
                    label_sequences.append(sub_seq_label)
        
        words_sequences = self.tokenize(words_sequences, load_tokenizer=load_tokenizer)
        self.save_tokenizer()
        self.index_word = self.tokenizer.index_word
        self.vocab_size = len(self.tokenizer.word_index) + 1
        self.words_sequences = words_sequences
        self.label_sequences = np.array(label_sequences).reshape((len(label_sequences), self.sequence_len))

        return self.words_sequences, self.label_sequences

    def sequences_to_texts(self, sequences):
        return self.tokenizer.sequences_to_texts(sequences)

    def save_tokenizer(self):
        print("Сохранение токенизатора")
        with open('tokenizer.pickle', 'wb') as file:
            pickle.dump(self.tokenizer, file)

    def load_tokenizer(self):
        print("Загрузка токенизатора")
        with open('tokenizer.pickle', 'rb') as file:
            self.tokenizer = pickle.load(file)