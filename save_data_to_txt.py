import os
import json

from preprocessing import Preprocess


if __name__ == "__main__":
	# Утилита перевода данных из формата json в текстовый формат в виде
	# -> Текст
	# -> Метки
	# ...
    with open("data/data.json", 'r', encoding='utf-8') as file:
        data = json.load(file)

    original_texts = [' '.join(drug['original']) for name, drug in data.items()]
    extracted_ades = [drug['extracted'] for name, drug in data.items()]

    preprocesser = Preprocess(sequence_len=10)
    print("Обработка текстов")
    prep_texts = preprocesser.preprocess(original_texts)

    print("Получение меток")
    labels = preprocesser.get_labels(prep_texts, extracted_ades)

    print("Сохранение обработанных данных")
    with open("data/preprocessed_data.txt", 'w', encoding='utf-8') as file:
        for text, label in zip(prep_texts, labels):
            text_line = ' '.join(text)
            label_line = ' '.join(map(lambda x: str(int(x)), label))
            file.write(text_line + '\n')
            file.write(label_line + '\n')