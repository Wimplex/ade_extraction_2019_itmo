import os, sys
import json, re
from preprocessing import Preprocess

import numpy as np
from sklearn.model_selection import train_test_split
from pymorphy2.analyzer import MorphAnalyzer

from keras.layers import Input, Dropout, Embedding
from keras.layers import Dense, LSTM, Bidirectional, GRU
from keras.models import Model, Sequential
from keras.optimizers import Adam
from keras.callbacks import LambdaCallback


DATA_DIR = "data/"
MODELS_DIR = "models/"

BATCH_SIZE = 128
EPOCHS = 300
LEARNING_RATE = 1e-3
DROPOUT = 0.2
WORD_DIM = 128
SEQUENCE_LEN = 15


def read_data(file_name):
    with open(os.path.join(DATA_DIR, file_name), 'r', encoding='utf-8') as file:
        data = json.load(file)
    return data


def read_marked_up_data(verbose=1):
    fpath1 = "data/lesha/"
    fpath2 = "data/katya/"
    
    # Чтение данных по размеченным мной текстам
    my_marked_up_lines = []
    with open(os.path.join(fpath1, "marked_up_data.txt"), 'r', encoding='utf-8') as file:
        my_marked_up_lines = list(map(lambda line: line.strip(), file.readlines()))
    
    count_of_marked_up_from_start = len(my_marked_up_lines)
    my_original_lines = []
    with open("data/preprocessed_data.txt", 'r', encoding='utf-8') as file:
        my_original_lines = file.readlines()[:2 * count_of_marked_up_from_start]
    my_original_lines = my_original_lines[::2]
    my_original_lines = list(map(lambda line: line.strip(), my_original_lines))
    
    if verbose:
        print("Строк обработано мной:", len(my_marked_up_lines))
        print([(len(m.split(' ')), len(l.split(' '))) for m, l in zip(my_marked_up_lines, my_original_lines)])

    # Чтение данных по размеченным Катей текстам
    katya_marked_up_lines = []
    with open(os.path.join(fpath2, "marked_up_data.txt"), 'r', encoding='utf-8') as file:
        katya_marked_up_lines = list(map(lambda line: line.strip(), file.readlines()))

    count_of_marked_up_from_start = len(katya_marked_up_lines)
    katya_original_lines = []
    with open("data/katya/preprocessed_data.txt", 'r', encoding='utf-8') as file:
        katya_original_lines = list(map(lambda line: line.strip(), file.readlines()[:2 * count_of_marked_up_from_start]))
    katya_original_lines = katya_original_lines[::2]

    if verbose:
        print("Строк обработано Катей:", len(katya_marked_up_lines))
        print([(len(m.split(' ')), len(l.split(' '))) for m, l in zip(katya_marked_up_lines, katya_original_lines)])

    labels = my_marked_up_lines + katya_marked_up_lines
    lines = my_original_lines + katya_original_lines
    return [line.split(' ') for line in lines], [list(map(lambda x: int(x) if x == '0' or x == 1 else 1, label.split(' '))) for label in labels]


class ADE_Extractor(Model):
    def __init__(self, name):
        super(ADE_Extractor, self).__init__()
        self.name = name
        self.model = None

    def build(self, input_shape):
        # Задаем архитектуру сети
        model = Sequential()
        model.add(Embedding(input_dim=input_shape[1], output_dim=WORD_DIM, input_length=input_shape[0]))

        model.add(Bidirectional(LSTM(256, return_sequences=True)))
        model.add(Bidirectional(LSTM(128, return_sequences=True)))
        model.add(Bidirectional(LSTM(64)))
        model.add(Dropout(rate=DROPOUT))

        model.add(Dense(64, activation='relu'))
        model.add(Dropout(rate=DROPOUT))

        model.add(Dense(input_shape[0], activation='softmax'))
        print(model.summary())

        # Компилируем модель с заданными ниже параметрами
        optimizer = Adam(lr=LEARNING_RATE)
        model.compile(optimizer=optimizer, loss='categorical_crossentropy', metrics=['accuracy'])
        self.model = model

    def fit(self, x, y, batch_size, epochs, validation_data, callbacks):
        try:
            self.model.fit(
                x=x,
                y=y,
                batch_size=batch_size, 
                epochs=epochs, 
                callbacks=callbacks, 
                validation_data=validation_data
            )
        finally:
            self.save()

    def predict(self, x):
        return self.model.predict(x)

    def save(self):
        print("Сохранение модели")
        self.model.save(
            os.path.join(MODELS_DIR, f"{self.name}_LR_{LEARNING_RATE}_EP_{EPOCHS}_DROP_{DROPOUT}_BATCH_{BATCH_SIZE}_WDIM_{WORD_DIM}.model")
        )


if __name__ == "__main__":
    USE_MARKED_UP_DATA = True

    if USE_MARKED_UP_DATA:
        lines, marked_up = read_marked_up_data()
        preprocesser = Preprocess(SEQUENCE_LEN)
        X, y = preprocesser.fit_on_prepared_data(lines, marked_up)
        X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.85)
        input_shape = (SEQUENCE_LEN, preprocesser.vocab_size)
    else:
        data = read_data("data.json")
        original_texts = [' '.join(drug['original']) for name, drug in data.items()]
        extracted_ades = [drug['extracted'] for name, drug in data.items()]

        # Предподготавливаем данные
        preprocesser = Preprocess(SEQUENCE_LEN)
        X, y = preprocesser.fit_on_texts(original_texts, extracted_ades, load_tokenizer=True)
        X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.8)
        input_shape = (SEQUENCE_LEN, preprocesser.vocab_size)


    # Создаем и настраиваем модель
    model = ADE_Extractor(name="better_extractor_with_manually_marked_up_data3")
    model.build(input_shape)

    # Создаем обработчик события окончания эпохи
    def on_epoch_end(epoch, logs):
        indexes = np.random.randint(0, X_test.shape[0], size=10)
        for i in indexes:
            x_ev, y_ev = X_test[i:i+1, :], y_test[i:i+1, :]
            print("\nОригинальная последоательность: %s" % preprocesser.sequences_to_texts(x_ev))
            print(" Оригинальные метки: %s" % np.uint8(y_ev)[0])
            pred = model.predict(x=x_ev)[0]
            print("Предсказанные метки: %s" % np.uint8(pred > 0.1))
            print()
    lambda_callback = LambdaCallback(on_epoch_end=on_epoch_end)

    # Обучаем модель
    model.fit(
        x=X_train,
        y=y_train,
        batch_size=BATCH_SIZE,
        epochs=EPOCHS,
        validation_data=(X_test, y_test),
        callbacks=[lambda_callback]
    )